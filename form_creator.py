from wtforms import Field, validators
from flask_wtf import FlaskForm

wtforms_module = __import__("wtforms")
validators_module = getattr(wtforms_module, "validators")


def create_form_class_from_yaml(form_name: str, form_config: dict) -> type:
    field_dict = {}
    for field_name, field_config in form_config.items():
        field_class = getattr(wtforms_module, field_config["type"])
        if not issubclass(field_class, Field):
            raise Exception("Invalid field_config type " + field_config["type"])

        validator_list = []
        if field_config["validators"]:
            for validator_name, validator_config in field_config["validators"].items():
                validator_class = getattr(validators_module, validator_name)
                validator_config = validator_config or {}
                validator = validator_class(**validator_config)
                validator_list.append(validator)

        field = field_class(field_config['label'], validators=validator_list)
        field_dict[field_name] = field
    new_class = type(f"{form_name}FormClass", (FlaskForm,), field_dict)
    return new_class
