# MQTTSettings
A small Flask based web app which will display a series of forms as defined in a YAML file, and post the results to a
given MQTT topic. Uses WTForms for form rendering and validation.

## Configuration
The configuration YAML file has the following sections.

### mqtt
This is a dictionary which should contain two child values: * *broker* for the URL of the broker
* *port* for the port to connect to the borker with

### forms
forms is a dictionary, with each form defined as a child dictionary. The name of the dictionary will form the URL of the form, accessable at `/forms/<form_name>`.

#### Field definitions
The children of each form dictionary correspond to fields on that form. These contain the following values:
* *label*: the name of the field, as it will be displayed to the user
* *type*: the field type. Must be one of the WTForms field types, as defined [here](https://github.com/wtforms/wtforms/blob/48cc05fedcf819546206c7c331a460c6c7e4a769/src/wtforms/fields/core.py#L17)
* *mqtt_topic*: the topic the result should be posted to on MQTT
* *validators*: _(optional)_ List of WTForms validators which should be applied to the field. Available validators are viewable [here](https://wtforms.readthedocs.io/en/2.3.x/validators/#built-in-validators).

##### Validator syntax
Each validator defined in the `validators` dictionary must be a dictionary of arguments for the parameter class. For example:
```yaml
example_string:
        label: String Field
        type: StringField
        mqtt_topic: set/example/example_string
        validators:
          Length:
            min: 1
            max: 1
          DataRequired:
```
The [Length validator](https://wtforms.readthedocs.io/en/2.3.x/validators/#wtforms.validators.Length) takes optional "min" and "max" parameters, so these are defined as key value pairs. The DataRequired validator is defined has no compulsory parameters, and so is defined as an empty dictionary.

Note that every validator can accept a "message" parameter to override the default error message
