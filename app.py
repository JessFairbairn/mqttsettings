import os
import logging
import yaml
from flask import abort, Flask, render_template
import paho.mqtt.client as mqtt

from form_creator import create_form_class_from_yaml

app = Flask(__name__)
logger = logging.getLogger()
SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY


with open("config/forms.yaml", "r") as stream:
    all_yaml = yaml.safe_load(stream)

mqtt_settings = all_yaml["mqtt"]
forms = all_yaml["forms"]

client = mqtt.Client("MQTTSettings")
try:
    client.connect(mqtt_settings.get("broker"), mqtt_settings.get("port", 1883))
except KeyError:
    logger.error("Broker URL not defined")
    raise


@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"

# class BlankForm(FlaskForm):
    # agile_region = StringField('Agile Region', [validators.Length(min=1, max=1), validators.DataRequired()])


@app.route("/forms/<form_name>", methods=['GET', 'POST'])
def form_page(form_name: str):
    try:
        form_definition = forms.get(form_name)
    except KeyError:
        return abort(404)

    FormClass = create_form_class_from_yaml(form_name, form_definition)
    form = FormClass()

    if form.validate_on_submit():
        for field_name, field_definition in form_definition.items():
            html_field = form[field_name]
            client.publish(field_definition["mqtt_topic"], html_field.data)
        return render_template('form.html', form=form, form_name=form_name)
    return render_template('form.html', form=form, form_name=form_name)
